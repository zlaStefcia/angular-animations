import { Component } from '@angular/core';

@Component({
  selector: 'app-card-enter-and-leave-css-and-ts',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardEnterAndLeaveCssAndTsComponent {
    public showCard = true;
    public leaveClassAttached = false;
    public toggleCard(): void {
        this.leaveClassAttached = !this.leaveClassAttached;
        setTimeout(
            () => {this.showCard = !this.showCard; },
            1000
        );
    }
}
