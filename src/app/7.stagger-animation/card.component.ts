import { Component } from '@angular/core';
import { trigger, style, animate, transition, query, stagger, keyframes } from '@angular/animations';

/*
First, we define a trigger, and then a transition of * => *, meaning, any state to any state.
Inside, we use the query method to grab all of the elements (in our case, it's the #list div's),
and on :enter ,(when they enter the DOM),we first set the styles to a 0 opacity.
This allows us to hide them initially.

We have a second query call after we've hidden the initial items,
where we call stagger for a duration of 300ms,
and then pass in our normal animate and keyframes functions to create a sequence-based animation.
*/

@Component({
    selector: 'app-stagger-animation',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    animations: [
        trigger('listAnimation', [
            transition('* => *', [
              query(':enter', style({ opacity: 0 })),
              query(':enter', stagger('300ms', [
                animate('1s ease-in', keyframes([
                  style({ opacity: 0, transform: 'translateY(-75%)', offset: 0 }),
                  style({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
                  style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
                ]))])),
            ])
        ]),
    ],
})
export class StaggerAnimationComponent {
    items = [
        'Hey this is an item',
        'Here is another one',
        'This is awesome'
    ];

    public pushItem(): void {
        this.items.push('Oh yeah that is awesome');
    }
}
