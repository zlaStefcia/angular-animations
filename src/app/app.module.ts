import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardEnterCssOnlyComponent } from './1.card-enter-css-only/card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CardEnterAndLeaveCssOnlyComponent } from './2.card-enter-and-leave-css-only/card.component';
import { CardEnterAndLeaveCssAndTsComponent } from './3.card-enter-and-leave-css-and-ts/card.component';
import { CardEnterAndLeaveNgAnimationsComponent } from './4.card-enter-and-leave-ng-animations/card.component';
import { MoreComplexNgAnimationsComponent } from './5.more-complex-ng-animations/card.component';
import { AnimationsLinkedWithStateComponent } from './6.animations-linked-with-state/card.component';
import { StaggerAnimationComponent } from './7.stagger-animation/card.component';
import { TwoAnimationsComponent } from './8.two-animations/card.component';

@NgModule({
  declarations: [
    AppComponent,
    CardEnterCssOnlyComponent,
    CardEnterAndLeaveCssOnlyComponent,
    CardEnterAndLeaveCssAndTsComponent,
    CardEnterAndLeaveNgAnimationsComponent,
    MoreComplexNgAnimationsComponent,
    AnimationsLinkedWithStateComponent,
    StaggerAnimationComponent,
    TwoAnimationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
