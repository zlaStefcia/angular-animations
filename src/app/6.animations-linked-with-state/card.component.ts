import { Component } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
    selector: 'app-animations-linked-with-state',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    animations: [
        trigger('animateArc', [
          state('true', style({
            left: '400px',
            top: '200px'
          })),
          state('false', style({
            left: '0',
            top: '200px'
          })),
          transition('false => true', animate('1000ms linear', keyframes([
            style({ left: '0',     top: '200px', offset: 0 }),
            style({ left: '200px', top: '100px', offset: 0.50 }),
            style({ left: '400px', top: '200px', offset: 1 })
          ]))),
          transition('true => false', animate('1000ms linear', keyframes([
            style({ left: '400px', top: '200px', offset: 0 }),
            style({ left: '200px', top: '100px', offset: 0.50 }),
            style({ left: '0',     top: '200px', offset: 1 })
          ])))
        ])
    ],
})
export class AnimationsLinkedWithStateComponent {
    arc = false;

    public toggleBounce(): void{
        this.arc = !this.arc;
    }
}
