import { Component } from '@angular/core';

@Component({
  selector: 'app-card-enter-css-only',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardEnterCssOnlyComponent {
    public showCard = true;
    public toggleCard(): void {
        this.showCard = !this.showCard;
    }
}
