import { Component } from '@angular/core';
import { trigger, style, animate, transition, query, stagger, keyframes } from '@angular/animations';

/*
First, we define a trigger, and then a transition of * => *, meaning, any state to any state.
Inside, we use the query method to grab all of the elements (in our case, it's the #list div's),
and on :enter ,(when they enter the DOM),we first set the styles to a 0 opacity.
This allows us to hide them initially.

We have a second query call after we've hidden the initial items,
where we call stagger for a duration of 300ms,
and then pass in our normal animate and keyframes functions to create a sequence-based animation.
*/

@Component({
    selector: 'app-two-animations',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    animations: [
        trigger('rotateAnimation', [
            transition(':enter', [
                style({ transform: 'rotate(360deg)', }),
                animate('1000ms ease-out', style({ transform: 'rotate(0)', })),
            ]),
        ]),
        trigger('colorAnimation', [
            transition(':enter', [
                style({ background: 'red', }),
                animate('1000ms ease-out', style({ background: 'green', })),
            ]),
        ]),
    ],
})
export class TwoAnimationsComponent {
    public showCard = true;
    public toggleCard(): void {
        this.showCard = !this.showCard;
    }
}
