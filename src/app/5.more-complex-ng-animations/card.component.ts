import { Component } from '@angular/core';
import { trigger, style, animate, transition, keyframes } from '@angular/animations';

@Component({
    selector: 'app-more-complex-ng-animations',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    animations: [
        trigger('enterLeaveAnimation', [
            transition(':enter', [
                style({ transform: 'translateX(-100%) scale(0) rotate(0)', opacity: 0, }),
                animate('2000ms ease-out', keyframes([
                    style(
                        {
                            transform: 'translateX(20%) scale(1.2)',
                            opacity: 0.8,
                            offset: 0.7
                        }
                    ),
                    style({ transform: 'translateX(-10%) scale(0.9)', opacity: 0.4, offset: 0.85 }),
                    style({ transform: 'translateX(0) scale(1) rotate(360deg)', opacity: 1, offset: 1 })
                ])),
            ]),
        ]),
    ],
})
export class MoreComplexNgAnimationsComponent {
    public showCard = true;
    public toggleCard(): void {
        this.showCard = !this.showCard;
    }
}
