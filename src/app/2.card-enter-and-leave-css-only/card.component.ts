import { Component } from '@angular/core';

@Component({
  selector: 'app-card-enter-and-leave-css-only',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardEnterAndLeaveCssOnlyComponent {
    public showCard = true;
    public leaveClassAttached = false;
    public toggleCard(): void {
        this.showCard = !this.showCard;
    }
}
