import { Component } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
    selector: 'app-card-enter-and-leave-ng-animations',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    animations: [
        trigger('enterLeaveAnimation', [
            // void => *
            transition(':enter', [
                style({ transform: 'translateX(-100%)', opacity: 0 }),
                animate('1000ms ease-out', style({ transform: 'translateX(0)', opacity: 1 })),
            ]),
            // * => void
            transition(':leave', [
                style({ transform: 'translateX(0)', opacity: 1 }),
                animate('1000ms ease-out', style({ transform: 'translateX(-100%)', opacity: 0 })),
            ]),
        ]),
    ],
})
export class CardEnterAndLeaveNgAnimationsComponent {
    public showCard = true;
    public toggleCard(): void {
        this.showCard = !this.showCard;
    }
}
